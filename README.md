### 根目录下先编译出目标文件main
`make`

### LZMA压缩文件
`./main d test.bin destin.bin` 

test.bin为要压缩的文件，destin.bin为即将压缩后的文件，文件名可自己随意定义


### LZMA解压文件
`./main e destin.bin encode.bin`

destin.bin为要解压的文件，encode.bin为即将解压后的文件，文件名可自己随意定义